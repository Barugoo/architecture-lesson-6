package main

import (
	"reflect"
	"testing"
)

func TestList_Pop(t *testing.T) {

	var listNotOk List
	var listOk List
	var aNode = &Node{}
	aNode.Data.SiteName = "Yandex"
	aNode.Data.URL = "ya.ru"

	listOk = List{Head: aNode, Tail: aNode}

	tests := []struct {
		name     string
		wantNode *Node
		wantErr  bool
	}{
		{"!ok", nil, true},
		{"ok", aNode, false},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			var (
				gotNode *Node
				err     error
			)

			switch tt.name {
			case "!ok":
				gotNode, err = listNotOk.Pop()
			default:
				gotNode, err = listOk.Pop()
			}

			if (err != nil) != tt.wantErr {
				t.Errorf("Pop() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotNode, tt.wantNode) {
				t.Errorf("Pop() gotNode = %v, want %v", gotNode, tt.wantNode)
			}
		})
	}
}
